/**
 * Example application to show using OpenCL in combination with CMake
 * This applications runs a simple kernel on all available devices
 *
 * Author: Matthias Bach <marix@marix.org>
 */

#include <iostream>
#include <cstdlib> // for exit


#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

using namespace std;

int main( int, char** )
{
	cl_int err;

	const cl_uint MAX_PLATFORMS = 4;
	cl_platform_id platforms[ MAX_PLATFORMS ];
	cl_uint nPlatforms = 0;
	cl_context context;
	cl_device_id devices[ 32 ];
	cl_uint nDevices;

	// try to get the platform (currently it should always be one)
	err = clGetPlatformIDs( MAX_PLATFORMS, platforms, &nPlatforms );
	if( err )
	{
		cerr << "Failed to get platforms" << endl;
		return -1;
	}

	if( ! nPlatforms )
	{
		cerr << "Failed to find an OpenCL platform" << endl;
		return -1;
	}

	if( nPlatforms > 1 )
	{
		cout << "Found more than one OpenCL platform, will use a random one." << endl;
		return -1;
	}

	// currently there is always only one
	// As the AMD implementation already supports ICD we have to specify the platform
	// we want to use
	cl_context_properties context_props[3] = {
	                                           CL_CONTEXT_PLATFORM,
	                                           (cl_context_properties) platforms[0],
	                                           0
	                                         };

	context = clCreateContextFromType( context_props, CL_DEVICE_TYPE_ALL, NULL, NULL, &err );
	if( err )
	{
		cerr << "Failed to create context." << endl;
		exit( -1 );
	}
	
	// get devices from context
	{
		size_t returned_size;
		err = clGetContextInfo( context, CL_CONTEXT_DEVICES, sizeof( devices ), devices, &returned_size);
		if( err )
		{
			cerr << "Failed to create context." << endl;
			exit( -1 );
		}
		
		nDevices = returned_size / sizeof( cl_device_id );
	}
		
	// loop over the devices to get their properties
	for( cl_uint j = 0; j < nDevices; ++j )
	{
		cl_device_type type;
		cl_uint vendor_id;
		cl_uint max_compute_units;
		size_t max_work_group_size;
		char name[ 64 ];
		size_t nName;
		cl_bool available;

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_TYPE, sizeof( type ), &type, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_TYPE" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_VENDOR_ID, sizeof( vendor_id ), &vendor_id, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_VENDOR_ID" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof( max_compute_units ), &max_compute_units, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_MAX_COMPUTE_UNITS" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof( max_work_group_size ), &max_work_group_size, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_MAX_WORK_GROUP_SIZE" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_NAME, sizeof( name ), name, &nName );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_NAME" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_AVAILABLE, sizeof( available ), &available, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_ENABLED" << endl;
			return -1;
		}

		if( ! available )
			continue;

		cout << name << " - ";
		switch( type )
		{
			case CL_DEVICE_TYPE_CPU:
				cout << "CPU";
				break;
			case CL_DEVICE_TYPE_GPU:
				cout << "GPU";
				break;
			case CL_DEVICE_TYPE_ACCELERATOR:
				cout << "ACCELERATOR";
				break;
			case CL_DEVICE_TYPE_DEFAULT:
				cout << "DEFAULT";
				break;
			default:
				cout << "UNKNOWN";
				break;
		}

		cout << " - " << max_compute_units << " x " << max_work_group_size << "\n";

		// TODO
		
		if( j != nDevices - 1 )
			cout << "\n";
		cout << endl;
	}

	err = clReleaseContext( context );
	if( err )
	{
		cerr << "Failed to release context" << endl;
		exit( -1 );
	}
	
	return 0;
}
