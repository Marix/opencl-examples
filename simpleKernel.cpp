/**
 * Example application to show using OpenCL in combination with CMake
 * This applications runs a simple kernel on all available devices
 *
 * Author: Matthias Bach <marix@marix.org>
 */

#include <iostream>
#include <cstdlib> // for exit


#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

using namespace std;

const char* PROGRAM_SOURCE = 
	"__kernel void add( unsigned int n, __write_only __global float* out, __read_only __global float* in_a, __read_only __global float* in_b )\n\
	{\n\
		int i = get_global_id(0);\n\
		if( i < n )\n\
			out[ i ] = in_a[ i ] + in_b[ i ];\n\
	}";

int main( int, char** )
{
	cl_int err;

	const cl_uint MAX_PLATFORMS = 4;
	cl_platform_id platforms[ MAX_PLATFORMS ];
	cl_uint nPlatforms = 0;

	cl_context context;
	cl_device_id devices[ 32 ];
	cl_uint nDevices;
	
	const cl_uint NUM_VALUES = 4096;
	float in_a[ NUM_VALUES ];
	float in_b[ NUM_VALUES ];
	float out[ NUM_VALUES ];
	
	cl_mem in_a_buffer, in_b_buffer, out_buffer;
	
	for( cl_uint i = 0; i < NUM_VALUES; ++i )
	{
		in_a[ i ] = 0.5f * i;
		in_b[ i ] = 0.5f * i;
	}

	// try to get the platform (currently it should always be one)
	err = clGetPlatformIDs( MAX_PLATFORMS, platforms, &nPlatforms );
	if( err )
	{
		cerr << "Failed to get platforms" << endl;
		return -1;
	}

	if( ! nPlatforms )
	{
		cerr << "Failed to find an OpenCL platform" << endl;
		return -1;
	}

	if( nPlatforms > 1 )
	{
		cout << "Found more than one OpenCL platform, will use a random one." << endl;
		return -1;
	}

	// currently there is always only one
	// As the AMD implementation already supports ICD we have to specify the platform
	// we want to use
	cl_context_properties context_props[3] = {
	                                           CL_CONTEXT_PLATFORM,
	                                           (cl_context_properties) platforms[0],
	                                           0
	                                         };

	context = clCreateContextFromType( context_props, CL_DEVICE_TYPE_ALL, NULL, NULL, &err );
	if( err )
	{
		cerr << "Failed to create context. " << err << endl;
		exit( -1 );
	}
	
	// get devices from context
	{
		size_t returned_size;
		err = clGetContextInfo( context, CL_CONTEXT_DEVICES, sizeof( devices ), devices, &returned_size);
		if( err )
		{
			cerr << "Failed to create context." << endl;
			exit( -1 );
		}
		
		nDevices = returned_size / sizeof( cl_device_id );
	}
		
	// loop over the devices to get their properties
	for( cl_uint j = 0; j < nDevices; ++j )
	{
		cl_device_type type;
		cl_uint vendor_id;
		cl_uint max_compute_units;
		size_t max_work_group_size;
		char name[ 64 ];
		size_t nName;
		cl_bool available;
		
		cl_command_queue commandQueue;
		cl_program program;
		cl_kernel kernel;

		char log[ 2048 ];
		size_t logSize;

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_TYPE, sizeof( type ), &type, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_TYPE" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_VENDOR_ID, sizeof( vendor_id ), &vendor_id, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_VENDOR_ID" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof( max_compute_units ), &max_compute_units, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_MAX_COMPUTE_UNITS" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof( max_work_group_size ), &max_work_group_size, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_MAX_WORK_GROUP_SIZE" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_NAME, sizeof( name ), name, &nName );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_NAME" << endl;
			return -1;
		}

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_AVAILABLE, sizeof( available ), &available, NULL );
		if( err )
		{
			cerr << "Failed to get device property - CL_DEVICE_ENABLED" << endl;
			return -1;
		}

		if( ! available )
			continue;

		cout << name << " - ";
		switch( type )
		{
			case CL_DEVICE_TYPE_CPU:
				cout << "CPU";
				break;
			case CL_DEVICE_TYPE_GPU:
				cout << "GPU";
				break;
			case CL_DEVICE_TYPE_ACCELERATOR:
				cout << "ACCELERATOR";
				break;
			case CL_DEVICE_TYPE_DEFAULT:
				cout << "DEFAULT";
				break;
			default:
				cout << "UNKNOWN";
				break;
		}

		cout << " - " << max_compute_units << " x " << max_work_group_size << "\n";

		
		commandQueue = clCreateCommandQueue( context, devices[ j ], NULL, &err );
		if( err )
		{
			cerr << "Failed to create command queue" << endl;
			exit(-1);
		}
		
		
		in_a_buffer = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof( in_a ), in_a, &err );
		if ( err ) {
			cerr << "Failed to create buffer object" << endl;
			exit(-1);
		}
		
		in_b_buffer = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof( in_b ), in_b, &err );
		if ( err ) {
			cerr << "Failed to create buffer object" << endl;
			exit(-1);
		}
		
		out_buffer = clCreateBuffer( context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof( out ), out, &err );
		if ( err ) {
			cerr << "Failed to create buffer object" << endl;
			exit(-1);
		}
		
		
		// no writing to buffer necessary!
		
		
		program = clCreateProgramWithSource( context,  1, &PROGRAM_SOURCE, NULL, &err );
		if( err )
		{
			cerr << "Failed to create program" << endl;
			exit(-1);
		}

		err = clBuildProgram( program, 0, NULL, "", NULL, NULL);
		if( err )
		{
			cerr << "Failed to build program:" << err << endl;
			
			err = clGetProgramBuildInfo( program, devices[ j ], CL_PROGRAM_BUILD_LOG, sizeof( log ), log, &logSize );
			if( err )
			{
				cerr << "Failed to get build log" << endl;
				exit(-1);
			}
			cout << log << '\n' << endl;
			
			exit(-1);
		}

		err = clGetProgramBuildInfo( program, devices[ j ], CL_PROGRAM_BUILD_LOG, sizeof( log ), log, &logSize );
		if( err )
		{
			cerr << "Failed to get build log" << endl;
			exit(-1);
		}
		cout << log << endl;

		kernel = clCreateKernel( program, "add", &err );
		if( err ) 
		{
			cerr << "Failed to create kernel" << endl;
			exit(-1);
		}
		
		err = clSetKernelArg( kernel, 0, sizeof( cl_uint ), &NUM_VALUES );
		if( err )
		{
			cerr << "Failed to set argument" << err << endl;
			exit(-1);
		}
		err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &out_buffer );
		if( err )
		{
			cerr << "Failed to set argument" << err << endl;
			exit(-1);
		}
		err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &in_a_buffer );
		if( err )
		{
			cerr << "Failed to set argument" << err << endl;
			exit(-1);
		}
		err = clSetKernelArg( kernel, 3, sizeof( cl_mem ), &in_b_buffer );
		if( err )
		{
			cerr << "Failed to set argument" << err << endl;
			exit(-1);
		}

		size_t workitems = NUM_VALUES;
		err = clEnqueueNDRangeKernel( commandQueue, kernel, 1, NULL, &workitems, NULL, 0, NULL, NULL );
		if( err )
		{
			cerr << "Failed to enqueue kernel" << err << endl;
			exit(-1);
		}

		err = clFlush( commandQueue );
		if( err )
		{
			cerr << "Failed to flush the command queue " << err << endl;
			exit(-1);
		}
		
		err = clFinish( commandQueue );
		if( err )
		{
			cerr << "Failed to wait for the command queue " << err << endl;
			exit(-1);
		}
		
		clReleaseKernel( kernel );
		if( err )
		{
			cerr << "Failed to release kernel" << endl;
			exit(-1);
		}
		
		// no reading from buffer necessary!
		
		err = clReleaseMemObject( out_buffer );
		if ( err ) {
			cerr << "Failed to release buffer object" << endl;
			exit(-1);
		}
		
		err = clReleaseMemObject( in_b_buffer );
		if ( err ) {
			cerr << "Failed to release buffer object" << endl;
			exit(-1);
		}
		
		err = clReleaseMemObject( in_a_buffer );
		if ( err ) {
			cerr << "Failed to release buffer object" << endl;
			exit(-1);
		}
		
		err = clReleaseCommandQueue( commandQueue );
		if( err )
		{
			cerr << "Failed to release command queue" << endl;
			exit(-1);
		}
		
		// check
		cl_bool correct = 1;
		for( cl_uint i = 0; i < NUM_VALUES; ++i )
		{
			// this is not a correct float comparison
			if( out[ i ] != ( in_a[ i ] + in_b[ i ] ) )
			{
				correct = 0;
				break;
			}
		}
		if( correct )
			cout << "Result correct" << endl;
		else
			cerr << "Result incorrect" << endl;

		
		if( j != nDevices - 1 )
			cout << "\n";
		cout << endl;
	}

	err = clReleaseContext( context );
	if( err )
	{
		cerr << "Failed to release context" << endl;
		exit( -1 );
	}
	
	return 0;
}
