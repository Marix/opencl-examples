/**
 * Example application to show using OpenCL in combination with CMake
 *
 * Author: Matthias Bach <marix@marix.org>
 */

#include <iostream>
#include <cstdlib> // for exit


#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

using namespace std;

int main( int, char** )
{
	cl_int err;

	// loop over all platforms
	const cl_uint MAX_PLATFORMS = 4;
	cl_platform_id platforms[ MAX_PLATFORMS ];
	cl_uint nPlatforms = 0;

	err = clGetPlatformIDs( MAX_PLATFORMS, platforms, &nPlatforms );
	if( err )
	{
		cerr << "Failed to get platforms" << endl;
		return -1;
	}

	for( int i = 0; i < nPlatforms; ++i )
	{
		const cl_uint MAX_DEVICES = 32;
		cl_device_id devices[ MAX_DEVICES ];
		cl_uint nDevices = 0;

		// get devices for this platform
		err = clGetDeviceIDs( platforms[ i ], CL_DEVICE_TYPE_ALL, MAX_DEVICES, devices, &nDevices );
		if( err )
		{
			cerr << "Failed to get devices" << endl;
			return -1;
		}

		// loop over the devices to get their properties
		for( int j = 0; j < nDevices; ++j )
		{
			cl_device_type type;
			cl_uint vendor_id;
			cl_uint max_compute_units;
			size_t max_work_group_size;
			char name[ 64 ];
			size_t nName;
			char vendor[ 64 ];
			size_t nVendor;
			char version[ 32 ];
			size_t nVersion;
			char extensions[ 512 ];
			size_t nExtensions;
			cl_bool available;

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_TYPE, sizeof( type ), &type, NULL );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_TYPE" << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_VENDOR_ID, sizeof( vendor_id ), &vendor_id, NULL );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_VENDOR_ID" << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof( max_compute_units ), &max_compute_units, NULL );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_MAX_COMPUTE_UNITS" << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof( max_work_group_size ), &max_work_group_size, NULL );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_MAX_WORK_GROUP_SIZE" << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_NAME, sizeof( name ), name, &nName );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_NAME" << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_VENDOR, sizeof( vendor ), vendor, &nVendor );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_VENDOR" << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_VERSION, sizeof( version ), version, &nVersion );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_VERSION" << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_EXTENSIONS, sizeof( extensions ), extensions, &nExtensions );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_EXTENSIONS " << err << endl;
				return -1;
			}

			err = clGetDeviceInfo( devices[ j ], CL_DEVICE_AVAILABLE, sizeof( available ), &available, NULL );
			if( err )
			{
				cerr << "Failed to get device property - CL_DEVICE_ENABLED" << endl;
				return -1;
			}

			if( ! available )
				cout << "N/A : ";

			cout << vendor << " " << name << " - OpenCL " << version << " - ";
			switch( type )
			{
				case CL_DEVICE_TYPE_CPU:
					cout << "CPU";
					break;
				case CL_DEVICE_TYPE_GPU:
					cout << "GPU";
					break;
				case CL_DEVICE_TYPE_ACCELERATOR:
					cout << "ACCELERATOR";
					break;
				case CL_DEVICE_TYPE_DEFAULT:
					cout << "DEFAULT";
					break;
				default:
					cout << "UNKNOWN";
					break;
			}

			cout << " - " << max_compute_units << " x " << max_work_group_size << " - ";
			cout << vendor_id << endl;
			if( nExtensions != 0 )
				cout << " - " << extensions << endl;
		}
	}

	return 0;
}
